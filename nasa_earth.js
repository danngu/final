var image_cell_template = "<div class='col-sm-3 image-cell'><div class='nasa-image'><img></div><div class='image-caption'></div><div class='image-coords'></div></div>";
var date_tmpl = "begin=YEAR-MONTH-DAY";
var myApiKey = "api_key=GTbbyLACvcJvyAyxnWIiYbNeRKPT4EOAjrUVCikN";

var example_image_url = "https://api.nasa.gov/EPIC/archive/natural/2015/06/13/png/epic_1b_20150613110250_01.png?api_key=YOUR_API_KEY";
var epic_natural_archive_base = "https://api.nasa.gov/EPIC/archive/natural/";
var api_url_query_base = "https://epic.gsfc.nasa.gov/api/natural/date/";

// ==========================================================
// START JS: synchronize the javascript to the DOM loading
// ==========================================================
$(document).ready(function() {
  $('#search_date').on('change', function () {
    var year = $("#search_date").val();
    console.log(year)
  }).change();
  // ========================================================
  // SECTION 1:  process on click events
  // ========================================================
  $('#get-images-btn').on('click', api_search);

  // process the "future" img element dynamically generated
  $("div").on("click", "img", function(){
    console.log(this.src);
    render_highres(this.src);
    // call render_highres() and display the high resolution
  });

  // ========================================================
  // TASK 1:  build the search AJAX call on NASA EPIC
  // ========================================================
  // Do the actual search in this function
  function api_search(e) {

    // get the value of the input search text box => date
    
    var date = $("#search_date").val();
    console.log(date);

    // build an info object to hold the search term and API key
    var info = {};
    var date_array = date.split('-');
    info.year = date_array[0];
    info.month = date_array[1];
    info.day = date_array[2];
    info.api_key = "GTbbyLACvcJvyAyxnWIiYbNeRKPT4EOAjrUVCikN";
    console.log(date_array);
    console.log(info);

    // build the search url and sling it into the URL request HTML element
    var search_url = api_url_query_base + date + "?" + myApiKey;
    console.log(search_url);
    // sling it!

    // make the jQuery AJAX call!
    $.ajax({
      url: search_url,
      success: function(data) {
        $('#reqURL').text(search_url);
        render_images(data, info);
      },
      cache: false
    });
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================

  // ========================================================
  // TASK 2: perform all image grid rendering operations
  // ========================================================
  function render_images(data,info) {
    // get NASA earth data from search results data
    console.log(data.length);
    var images = [];
    for (var i = 0; i < data.length; i++) {
      // build an array of objects that captures all of the key image data
      // => image url
      images[i] = {};
      images[i].url = data[i].image;
      // => centroid coordinates to be displayed in the caption area
      images[i].lat = data[i].centroid_coordinates.lat;
      images[i].lon = data[i].centroid_coordinates.lon;
      // => image date to be displayed in the caption area (under thumbnail)
      images[i].date = data[i].date;
    }
    console.log(images);

    // select the image grid and clear out the previous render (if any)
    var earth_dom = $('#image-grid');
    earth_dom.empty();

    // render all images in an iterative loop here!
    for (var i = 0; i < images.length; i++) {
      
      var epic_natural = "https://api.nasa.gov/EPIC/archive/natural/" + info.year + "/" + info.month + "/" + info.day + "/png/" + images[i].url + ".png?" + myApiKey;
      
      if (i % 4 == 0 && i > 0) {
        earth_dom.append("</div>");
        console.log(i);
      }
      if (i % 4 == 0) {
        earth_dom.append("<div class='row'>");
        console.log(i);
      }
      var img = "<div class='col-sm-3' image-cell><div class='nasa-image'><img src='" + epic_natural + "'></div><div class='image-caption'>" + images[i].date + "</div><div class='image-coords'>" + "lat:" + images[i].lat + " lon:" + images[i].lon + "</div></div>"
      earth_dom.append(img);
      
      $('#imgURL').text(epic_natural);
    }

    earth_dom.append("</div>");

  }

  // ========================================================
  // TASK 3: perform single high resolution rendering
  // ========================================================
  // function to render the high resolution image
  function render_highres(src) {
    // use jQuery to select and maniupate the portion of the DOM => #image-grid
    //  to insert your high resolution image
    var earth_dom = $('#image-grid');
    var boo = src;
    console.log(boo);
    earth_dom.empty();
    earth_dom.append("<div class='row'><div class='col-sm-12'><img src='" + boo + "'></div></div>");
  }
  // ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
  // ========================================================
});
